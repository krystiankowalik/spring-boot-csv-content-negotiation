package com.gitlab.krystiankowalik.contentnegotiation.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.gitlab.krystiankowalik.contentnegotiation.model.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CsvConfig {

    @Bean
    ObjectWriter personCsvWriter() {
        final CsvMapper csvMapper = new CsvMapper();
        csvMapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);

        return csvMapper
                .writerFor(Person.class)
                .with(csvMapper.schemaFor(Person.class));
    }
}

package com.gitlab.krystiankowalik.contentnegotiation.config;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.gitlab.krystiankowalik.contentnegotiation.model.Person;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Configuration
public class CsvMessageConverter extends AbstractHttpMessageConverter<Person> {
    public static final MediaType MEDIA_TYPE = new MediaType("text", "csv", StandardCharsets.UTF_8);

    private final ObjectWriter personCsvWriter;

    public CsvMessageConverter(ObjectWriter personCsvWriter) {
        super(MEDIA_TYPE);
        this.personCsvWriter = personCsvWriter;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return Person.class.equals(clazz);
    }

    @Override
    protected Person readInternal(Class<? extends Person> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return null;
    }

    @Override
    protected void writeInternal(Person response, HttpOutputMessage output) throws IOException, HttpMessageNotWritableException {
        output.getHeaders().setContentType(MEDIA_TYPE);
        final OutputStream out = output.getBody();
        out.write(personCsvWriter.writeValueAsBytes(response));
        out.close();
    }

    @Override
    public List<MediaType> getSupportedMediaTypes(Class<?> clazz) {
        List<MediaType> supportedMediaTypes = super.getSupportedMediaTypes(clazz);
        supportedMediaTypes.add(MEDIA_TYPE);
        return supportedMediaTypes;
    }
}
package com.gitlab.krystiankowalik.contentnegotiation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
@JsonPropertyOrder({"id", "firstName", "lastName", "email"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
}

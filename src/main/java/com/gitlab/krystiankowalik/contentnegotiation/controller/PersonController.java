package com.gitlab.krystiankowalik.contentnegotiation.controller;

import com.gitlab.krystiankowalik.contentnegotiation.model.Person;
import com.gitlab.krystiankowalik.contentnegotiation.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PersonController {

    private final PersonRepository repository;

    @GetMapping(value = "/persons", produces = {"application/json", "text/csv"})
    public Person getPerson() {
        return repository.get();
    }
}

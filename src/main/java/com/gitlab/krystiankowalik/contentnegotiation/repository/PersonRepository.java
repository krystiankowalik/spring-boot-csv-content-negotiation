package com.gitlab.krystiankowalik.contentnegotiation.repository;

import com.gitlab.krystiankowalik.contentnegotiation.model.Person;

public interface PersonRepository {

    Person get();
}

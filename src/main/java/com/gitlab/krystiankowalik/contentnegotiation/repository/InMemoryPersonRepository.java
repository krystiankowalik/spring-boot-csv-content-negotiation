package com.gitlab.krystiankowalik.contentnegotiation.repository;

import com.gitlab.krystiankowalik.contentnegotiation.model.Person;
import org.springframework.stereotype.Repository;

@Repository
public class InMemoryPersonRepository implements PersonRepository {

    private static final Person SAMPLE =
            Person.builder().id(1).firstName("John").lastName("Smith").email("john@smith.com").build();

    @Override
    public Person get() {
        return SAMPLE;
    }
}

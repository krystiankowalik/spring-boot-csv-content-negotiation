## CSV output
### Request
```shell
curl --location --request GET 'localhost:8081/persons' \
--header 'Accept: text/csv'
```

### Response
```text
1,John,Smith,john@smith.com
```
## JSON output

### Request
```shell
curl --location --request GET 'localhost:8081/persons' \
--header 'Accept: application/json'
```
### Response

```json
{
  "id": 1,
  "firstName": "John",
  "lastName": "Smith",
  "email": "john@smith.com"
}
```

